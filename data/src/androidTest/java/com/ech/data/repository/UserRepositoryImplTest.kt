package com.ech.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.ech.data.local.AppDatabase
import com.ech.data.local.dao.UserDao
import com.ech.domain.model.User
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import com.google.common.truth.Truth.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class UserRepositoryImplTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var dao: UserDao

    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.getUserDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun addUser() = runBlockingTest {

        val user = User()
        user.id = 1
        user.name = "Srikanth"
        user.description = "Some Description"
        user.email = "shrikanthmca@gmail.com"
        user.bitCoinWalletId = "BitXXX"
        user.bitCoinWalletBalance = "10000"
        user.ethereumWalletId = "EitYYY"
        user.ethereumWalletBalance = "9999"
        user.maxTransactionAmount = "19000"

        dao.insert(user)

        val allUsers = dao.getAllUsers()
        assertThat(allUsers).contains(user)
    }

    @Test
    fun getUser() = runBlockingTest {

        val user = User()
        user.id = 1
        user.name = "Srikanth"
        user.description = "Some Description"
        user.email = "shrikanthmca@gmail.com"
        user.bitCoinWalletId = "BitXXX"
        user.bitCoinWalletBalance = "10000"
        user.ethereumWalletId = "EitYYY"
        user.ethereumWalletBalance = "9999"
        user.maxTransactionAmount = "19000"

        dao.insert(user)

        val userDetail = dao.getUser(1)
        assertThat(userDetail.name).isEqualTo("Srikanth")
    }
}