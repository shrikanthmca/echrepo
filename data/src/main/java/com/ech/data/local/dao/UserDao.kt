package com.ech.data.local.dao

import androidx.room.*
import com.ech.domain.model.User

/**
 * it provides access to [User] underlying database
 * */
@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User): Long

    @Query("SELECT * FROM User")
    fun getAllUsers(): MutableList<User>

    @Query("SELECT * FROM User WHERE id=:userId")
    fun getUser(userId: Long): User

    @Delete
    fun delete(user: User)

    @Query("DELETE  FROM User WHERE id=:id")
    fun deleteUser(id: Long)

    @Query("DELETE FROM User")
    fun deleteAll()

    @Update
    fun update(user: User)

}