package com.ech.data.repository

import com.ech.data.local.AppDatabase
import com.ech.domain.model.User
import com.ech.domain.repositories.UserRepository

/**
 * This repository is responsible for
 * fetching data from server or db
 * */
class UserRepositoryImpl(
    private val database: AppDatabase
) : UserRepository {

    override suspend fun getUser(userId: Long?): User {
        return userId?.let { database.getUserDao().getUser(it) }!!
    }

    override suspend fun deleteUser(user: User) {
        database.getUserDao().delete(user)
    }

    override suspend fun deleteUser(user: Long) {
        database.getUserDao().deleteUser(user)
    }

    override suspend fun addUser(user: User) {
        database.getUserDao().insert(user)
    }

    override suspend fun updateUser(user: User) {
        database.getUserDao().update(user)
    }

    override suspend fun loadAllUsers(): List<User> {
        return database.getUserDao().getAllUsers()
    }

}