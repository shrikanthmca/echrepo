package com.ech.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "User")
data class User(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    @ColumnInfo(name = "Customer Name") var name: String? = null,
    @ColumnInfo(name = "Description") var description: String? = null,
    @ColumnInfo(name = "Email") var email: String? = null,
    @ColumnInfo(name = "Bitcoin Wallet ID") var bitCoinWalletId: String? = null,
    @ColumnInfo(name = "Bitcoin Wallet balance") var bitCoinWalletBalance: String? = null,
    @ColumnInfo(name = "Ethereum Wallet Id") var ethereumWalletId: String? = null,
    @ColumnInfo(name = "Ethereum Wallet balance ") var ethereumWalletBalance: String? = null,
    @ColumnInfo(name = "Max Transaction Amount") var maxTransactionAmount: String? = null
)