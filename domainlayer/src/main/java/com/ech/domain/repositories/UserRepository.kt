package com.ech.domain.repositories

import com.ech.domain.model.User

interface UserRepository {

    suspend fun getUser(userId: Long?): User

    suspend fun deleteUser(user: User)

    suspend fun deleteUser(user: Long)

    suspend fun addUser(user: User)

    suspend fun updateUser(user: User)

    suspend fun loadAllUsers(): List<User>

}