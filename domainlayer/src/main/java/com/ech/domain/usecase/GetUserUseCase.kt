package com.ech.domain.usecase


import com.ech.domain.model.User
import com.ech.domain.repositories.UserRepository

class GetUserUseCase constructor(private val repository: UserRepository) {

    private var user: User? = null

    suspend fun saveUser(value: User) {
        user = value
        user?.let { repository.addUser(it) }
    }

    suspend fun deleteUser(value: Long) {
        repository.deleteUser(value)
    }

    suspend fun updateUser(value: User) {
        repository.updateUser(value)
    }

    suspend fun loadAllUsers(): List<User> {
        return repository.loadAllUsers()
    }
}