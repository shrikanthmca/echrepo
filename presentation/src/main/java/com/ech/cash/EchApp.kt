package com.ech.cash

import android.app.Application
import com.ech.cash.di.*
import org.koin.android.ext.android.startKoin

class EchApp : Application() {

    override fun onCreate() {
        super.onCreate()
        loadKoinDi()
    }

    private fun loadKoinDi() {
        startKoin(
            this, listOf(
                mNetworkModules,
                mRepositoryModules,
                mViewModels,
                mUseCaseModules,
                databaseModule
            )
        )
    }

}