package com.ech.cash.di

import com.ech.cash.BuildConfig
import com.ech.cash.networking.createNetworkClient
import com.ech.cash.viewmodel.DBViewModel
import com.ech.data.local.AppDatabase
import com.ech.data.repository.UserRepositoryImpl
import com.ech.domain.repositories.UserRepository
import com.ech.domain.usecase.GetUserUseCase
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

private const val RETROFIT_INSTANCE = "Retrofit"

val mViewModels = module {
    viewModel {
        DBViewModel(userUseCase = get())
    }
}

val mNetworkModules = module {
    single(name = RETROFIT_INSTANCE) { createNetworkClient(BuildConfig.BASE_URL) }
}

val mRepositoryModules = module {
    single { UserRepositoryImpl(get()) as UserRepository }
}

val mUseCaseModules = module {
    factory {
        GetUserUseCase(
            repository = get()
        )
    }
}

val databaseModule = module {
    single { AppDatabase.getInstance(get()) }
    single { get<AppDatabase>().getUserDao() }
}


