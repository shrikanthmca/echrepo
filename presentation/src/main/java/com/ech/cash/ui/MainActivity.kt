package com.ech.cash.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ech.cash.R
import com.ech.cash.viewmodel.DBViewModel
import com.ech.domain.model.User
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.user_form.*
import org.koin.android.ext.android.inject
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    private var userDetailsAdapter = UserDetailsAdapter()
    private val dbViewModel: DBViewModel by inject()
    private val mUser = User()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler_view.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_view.adapter = userDetailsAdapter

        editTextChangeListeners()

        //New User
        btn_new_user.setOnClickListener {
            user_form.visibility = View.VISIBLE
            recycler_view.visibility = View.GONE
        }

        btn_show_data.setOnClickListener {
            recycler_view.visibility = View.VISIBLE
            user_form.visibility = View.GONE
            dbViewModel.loadAllUsers()
        }

        btn_save.setOnClickListener {
            if (isValidData()) {
                Executors.newSingleThreadExecutor().execute {
                    dbViewModel.addUser(mUser)
                }
            }
        }

        dbViewModel.getUsers().observe(this, Observer {
            userDetailsAdapter.setData(it)
        })
    }

    private fun editTextChangeListeners() {

        tie_id.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_id.error = null
            }
        })

        tie_name.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_name.error = null
            }
        })

        tie_desc.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_desc.error = null
            }
        })

        tie_email.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_email.error = null
            }
        })

        tie_bitcoin_wallet_id.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_bitcoin_wallet_id.error = null
            }
        })

        tie_bitcoin_balance.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_bitcoin_balance.error = null
            }
        })

        tie_ethereum_id.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_ethereum_id.error = null
            }
        })

        tie_ethereum_balance.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_ethereum_balance.error = null
            }
        })

        tie_max_amount.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                til_max_amount.error = null
            }
        })

    }

    private fun isValidData(): Boolean {
        //Id
        if (tie_id.text.toString().isEmpty()) {
            til_id.error = getString(R.string.cannot_be_blank)
            return false
        } else {
            mUser.id = tie_id.text.toString().toLong()
        }
        //Name
        if (tie_name.text.toString().isEmpty()) {
            til_name.error = getString(R.string.cannot_be_blank)
            return false
        } else {
            mUser.name = tie_name.text.toString()
        }
        //Description
        if (tie_desc.text.toString().isEmpty()) {
            til_desc.error = getString(R.string.cannot_be_blank)
            return false
        } else {
            mUser.description = tie_desc.text.toString()
        }
        //Email
        if (tie_email.text.toString().isEmpty()) {
            til_email.error = getString(R.string.cannot_be_blank)
            return false
        } else {
            mUser.email = tie_email.text.toString()
        }
        //Bitcoin wallet ID
        if (tie_bitcoin_wallet_id.text.toString().isEmpty()) {
            til_bitcoin_wallet_id.error = getString(R.string.cannot_be_blank)
            return false
        } else {
            mUser.bitCoinWalletId = tie_bitcoin_wallet_id.text.toString()
        }
        //Bitcoin balance
        if (tie_bitcoin_balance.text.toString().isEmpty()) {
            til_bitcoin_balance.error = getString(R.string.cannot_be_blank)
            return false
        } else {
            mUser.bitCoinWalletBalance = tie_bitcoin_balance.text.toString()
        }
        //Etherum id
        if (tie_ethereum_id.text.toString().isEmpty()) {
            til_ethereum_id.error = getString(R.string.cannot_be_blank)
            return false
        } else {
            mUser.ethereumWalletId = tie_ethereum_id.text.toString()
        }
        //Etherum Balance
        if (tie_ethereum_balance.text.toString().isEmpty()) {
            til_ethereum_balance.error = getString(R.string.cannot_be_blank)
            return false
        } else {
            mUser.ethereumWalletBalance = tie_ethereum_balance.text.toString()
        }
        //Max Amount
        if (tie_max_amount.text.toString().isEmpty()) {
            til_max_amount.error = getString(R.string.cannot_be_blank)
            return false
        } else {
            mUser.maxTransactionAmount = tie_max_amount.text.toString()
        }

        return true
    }

}
