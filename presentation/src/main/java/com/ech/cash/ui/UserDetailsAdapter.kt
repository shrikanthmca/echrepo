package com.ech.cash.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ech.cash.R
import com.ech.domain.model.User
import kotlinx.android.synthetic.main.user_row.view.*

class UserDetailsAdapter : RecyclerView.Adapter<UserDetailsAdapter.UserViewHolder>() {

    var listOfUsers = mutableListOf<User>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.user_row, parent, false)
        return UserViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listOfUsers.size
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(listOfUsers[position])
    }

    fun setData(listOfUsers: List<User>) {
        this.listOfUsers = listOfUsers as MutableList<User>
        notifyDataSetChanged()
    }

    class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(user: User) {
            with(itemView) {
                tv_id.text = user.id.toString()
                tv_name.text = user.name.toString()
                tv_description.text = user.description.toString()
                tv_email.text = user.email.toString()
                tv_bitcoin_wallet_id.text = user.bitCoinWalletId.toString()
                tv_bitcoin_wallet_balance.text = user.bitCoinWalletBalance.toString()
                tv_etherum_wallet_id.text = user.ethereumWalletId.toString()
                tv_etherum_wallet_balance.text = user.ethereumWalletBalance.toString()
                tv_max_amount.text = user.maxTransactionAmount.toString()
            }
        }
    }
}