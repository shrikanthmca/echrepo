package com.ech.cash.viewmodel


import androidx.lifecycle.MutableLiveData
import com.ech.domain.model.User
import com.ech.domain.usecase.GetUserUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DBViewModel(private val userUseCase: GetUserUseCase) : BaseViewModel() {

    private val mutableUserList = MutableLiveData<List<User>>()

    fun addUser(user: User) {
        launch {
            withContext(Dispatchers.IO) {
                userUseCase.saveUser(user)
            }
        }
    }

    fun updateUser(user: User) {
        launch {
            withContext(Dispatchers.IO) { userUseCase.updateUser(user) }
        }
    }

    fun loadAllUsers() {
        launch {
            val result = withContext(Dispatchers.IO) {
                userUseCase.loadAllUsers()
            }
            mutableUserList.value = result
        }
    }

    fun getUsers() = mutableUserList
}