package com.ech.cash.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ech.TestCoroutineRule
import com.ech.domain.model.User
import com.ech.domain.repositories.UserRepository
import com.ech.domain.usecase.GetUserUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class DBViewModelTest {

    @Rule
    @JvmField
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var viewModel: DBViewModel

    @Mock
    private lateinit var userCase: GetUserUseCase

    @Mock
    private lateinit var repository: UserRepository

    @Mock
    private lateinit var userObserver: Observer<List<User>>

    @Test
    fun testAddUser() {

        //given
        val user = User()
        user.id = 1
        user.name = "Srikanth"
        user.description = "Some Description"
        user.email = "shrikanthmca@gmail.com"
        user.bitCoinWalletId = "BitXXX"
        user.bitCoinWalletBalance = "10000"
        user.ethereumWalletId = "EitYYY"
        user.ethereumWalletBalance = "9999"
        user.maxTransactionAmount = "19000"

        //when
        userCase = GetUserUseCase(repository)
        viewModel = DBViewModel(userCase)
        viewModel.addUser(user)

        //then
        testCoroutineRule.runBlockingTest {
            doReturn(emptyList<User>()).`when`(repository).loadAllUsers()
            userCase.loadAllUsers()
            verify(repository).loadAllUsers()
        }

    }
}